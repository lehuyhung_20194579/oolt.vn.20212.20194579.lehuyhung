package com.oolt.labs;

import java.util.Scanner;
import java.util.Arrays;

public class MyDate {
	// Khai bao 3 thuoc tinh
	private int day;
	private int month;
	private int year;
	
	String[] arr1 = {"January","February","March","April","May","June",
			"July","August","September","October","November","December"};
	String[] arr2 = {"1st","2nd","3rd","4th","5th","6th","7th","8th","9th","10th",
			"11th","12th","13th","14th","15th","16th","17th","18th","19th","20th",
			"21th","22th","23th","24th","25th","26th","27th","28th","29th","30th","31st"};
	// b, Cac phuong thuc getter / setter
	public int getDay() {
		return this.day;
	}
	
	public void setDay (int day) {
		if (day > 0 && day <= 31)
			this.day = day;
		else this.day = 16;
	}
	
	public int getMonth() {
		return this.month;
	}
	
	public void setMonth (int month) {
		if (month > 0 && month <= 12)
			this.month = month;
		else this.month = 4;
	}
	
	public int getYear() {
		return this.year;
	}
	
	public void setYear (int year) {
		if (year > 0 && year <= 2022)
			this.year = year;
		else this.year = 2022;
	}
	// a, Phuong thuc khoi tao
	// Khong tham so để 3 thuoc tinh la ngay hien tai
	public MyDate(){
		this.day = 16;
		this.month = 4;
		this.year = 2022;
	}
	
	// Cả 3 tham so
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	// 1 tham so dai dien
	public MyDate(String str) {
		String[] splits = str.split(" ");
		String str1 = splits [0];
		String str2 = splits [1];
		String str3 = splits [2];
		
		for(int i = 0; i < 12; i++)
			if (str1.compareTo(arr1[i]) == 0) {
				this.month = i+1;
			}
		
		for(int i = 0; i < 31; i++)
			if (str2.compareTo(arr2[i]) == 0) this.day = i+1;
		
		this.year = Integer.parseInt(str3);
	}
	
	// c, Write  a  method  named accept() which  asks  users  to  enter  a  date(String)from keyboard. 
	// Set corresponding values for the three attributes of MyDate.
	public void accept() {
		Scanner scanner = new Scanner(System.in);
		String str;
		System.out.println("Enter A Date: ");
		str = scanner.nextLine();
		String[] splits = str.split(" ");
		String str1 = splits [0];
		String str2 = splits [1];
		String str3 = splits [2];
		
		for(int i = 0; i < 12; i++)
			if (str1.compareTo(arr1[i]) == 0) {
				this.month = i+1;
			}
		
		for(int i = 0; i < 31; i++)
			if (str2.compareTo(arr2[i]) == 0) this.day = i+1;
		
		this.year = Integer.parseInt(str3);
		scanner.close();
		
	}
	
	// d, Write a method named print()which print the current date to the screen.
	public void print() {
		System.out.println("---------------------------------------------------");
		System.out.println("Ngay hien tai: " + day + "/" + month + "/" + year + " Hoac " + arr1[month-1] + " " + arr2[day-1] + " " + year);
	}
	
}