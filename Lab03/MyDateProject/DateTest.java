package com.oolt.labs;

public class DateTest {

	public static void main(String[] args) {
		MyDate date1 = new MyDate();
		MyDate date2 = new MyDate(28,11,2001);
		MyDate date3 = new MyDate("February 14th 2022");
		date1.print();
		date2.print();
		date3.print();
		MyDate date4 = new MyDate();
		date4.accept();
		date4.print();

	}

}