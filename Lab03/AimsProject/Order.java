package com.oolt.labs;

public class Order {
	//Khai bao mot hang so: so luong san pham toi da
	public static final int MAX_NUMBERS_ORDERED = 10;
	//Khai bao mot mang cac doi tuong DVD 
	private DigitalVideoDisc itemsOrdered[] = new DigitalVideoDisc[MAX_NUMBERS_ORDERED];
	//Khai bao thuoc tinh chua so luong san pham hien co trong don hang
	private int QtyOrdered = 0;
	
	//Xay dung cac phuong thuc
	//Cac phuong thuc getter / setter
	public int getQtyOrdered() {
		return QtyOrdered;
	}
	public void setQtyOrdered(int qtyOrdered) {
		if(QtyOrdered >= 0)
		    this.QtyOrdered = qtyOrdered;
	}
	
	//Phuong thuc them mot doi tuong DVD vao don hang
	//Chinh la them mot doi tuong DVD vao mang
	//--> Phai kiem tra xem mang da bi day chua
	public void addDigitalVideoDisc(DigitalVideoDisc disc) {
		if(this.QtyOrdered == MAX_NUMBERS_ORDERED)
			System.out.println("The Order Is Almost Full");
		else {
			this.itemsOrdered[QtyOrdered] = disc;
			QtyOrdered++;
			System.out.println("The Disc Has Been Added");
			System.out.println("Total disc: " + this.QtyOrdered);
		}
	}
	public void removeDigitalVideoDisc(DigitalVideoDisc disc) {
		//Viet lenh xu ly loai bo doi tuong khoi mang
		//Luu y kiem tra mang khong co phan tu
		int d = 0;
		if (this.QtyOrdered == 0)
			System.out.println("The Order Is NULL");
		else {
			for(int i = 0; i< this.QtyOrdered ; i++)
				if (this.itemsOrdered[i] == disc) {
					d = d+1;
					for(int j = i; j< this.QtyOrdered ; j++)
					this.itemsOrdered[j] = this.itemsOrdered[j + 1];
					this.QtyOrdered = this.QtyOrdered-1;
					System.out.println("The Disc Has Been Removed");
					System.out.println("Total disc: " + this.QtyOrdered);
				}
			if (d == 0) 
				System.out.println("This Order Has't This Disc!");
		}
	}
	
	//Phuong thuc tinh tong gia tri don hang
	public float totalCost() {
		float total = 0.0f;
		for(int i=0;i < this.QtyOrdered;i++) {
			total += itemsOrdered[i].getCost();
		}
		return total;
	}
	

}