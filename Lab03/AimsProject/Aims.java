package com.oolt.labs;

public class Aims {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
        /*DigitalVideoDisc dvd1 = new DigitalVideoDisc();
        DigitalVideoDisc dvd2 = new DigitalVideoDisc(
        		"Mua thu Ha Noi");*/
		DigitalVideoDisc dvd1 = new DigitalVideoDisc(
				"The Lion King");
		dvd1.setCategory("Animation");
		dvd1.setDirector("Roger Allers");
		dvd1.setLength(87);
		dvd1.setCost(19.95f);
		
		//dvd1.printInfo();
		DigitalVideoDisc dvd2 = new DigitalVideoDisc(
				"Star Wars","Science Fiction","George Lucas",
				124,24.95f);
		DigitalVideoDisc dvd3 = new DigitalVideoDisc(
				"Aladin","Animation","John Musker",
				90,18.99f);
		dvd1.printInfo();
		dvd2.printInfo();
		dvd3.printInfo();
		System.out.println("---------------------------");
		//Tao mot don hang moi
		Order anOrder = new Order();
		
		//Them cac dia DVD vao don hang
		anOrder.addDigitalVideoDisc(dvd1);
		anOrder.addDigitalVideoDisc(dvd2);
		//anOrder.addDigitalVideoDisc(dvd3);
		//In ra tong gia tri don hang
		System.out.println("Total cost is: " + 
		      anOrder.totalCost());
		//Xoa mot DVD ra khoi don hang
		anOrder.removeDigitalVideoDisc(dvd3);
		//In ra tong gia tri don hang
		System.out.println("Total cost is: " + 
			  anOrder.totalCost());

	}

}