package com.oolt.labs;

public class DigitalVideoDisc {
    //Khai bao cac thuoc tinh
	private String title;
	private String category;
	private String director;
	private int length;
	private float cost;
	
	//Xay dung cac phuong thuc
	//Cac phuong thuc getter / setter
	public String getTitle() {
		return this.title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	//Length lon hon 0
	public void setLength(int length) {
		if(length > 0)
		    this.length = length;
		else this.length = 0;
	}
	public float getCost() {
		return cost;
	}
	public void setCost(float cost) {
		if(cost>0)
		    this.cost = cost;
		else this.cost = 0.0f;
	}
	
	// Các phương thức khởi tạo (constructor)
	//-Dac diem cua constructor
	//+Ten trung voi ten lop
	//+Khong co kieu tra ve, khong co tu khoa void
	//+Xau dung nhieu constructor giup khoi tao doi tuong từ lớp theo nhieu cach khac nhau
	//1. Constructor không tham số
	public DigitalVideoDisc() {
		this.title = "";
		this.category = "";
		this.director = "";
		this.length = 0;
		this.cost = 0.0f;
	}
	//2. Constructor có 1 tham số: Create DVD object by title
	public DigitalVideoDisc(String title) {
		this.title = title;
	}
	//3. Constructor co 2 tham so
	public DigitalVideoDisc(String title, String category) {
		this.title = title;
		this.category = category;
	}
	//4. Constructor co 3 tham so
	public DigitalVideoDisc(String title, String category, String director) {
		this.title = title;
		this.category = category;
		this.director = director;
	}
	//5. Constructor with all attributes
	public DigitalVideoDisc(String title, String category, String director, int length, float cost) {
		super();
		this.title = title;
		this.category = category;
		this.director = director;
		this.length = length;
		this.cost = cost;
	}
	//Cac phuong thuc khac
	//In thong tin cua doi tuong dvd
	public void printInfo() {
		System.out.println("-------DVD Info-------");
		System.out.println("Title: " + this.title);
		System.out.println("Category: " + this.category);
		System.out.println("Director: " + this.director);
		System.out.println("Length: " + this.length);
		System.out.println("Cost: " + this.cost);
	}
	
}
