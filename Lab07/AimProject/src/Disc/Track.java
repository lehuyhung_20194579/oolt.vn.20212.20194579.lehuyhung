package Disc;

import hust.soict.hedspi.aims.media.Playable;

public class Track  implements Playable{
    private String title;
    private int length;
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    public Track(String title, int length) {
        this.title = title;
        this.length = length;
    }
    public Track(){

    }
    @Override
    public void play() {
        System.out.println("Playing Track: " + this.getTitle());
        System.out.println("Track length:  " + this.getLength());
    }
    
}
