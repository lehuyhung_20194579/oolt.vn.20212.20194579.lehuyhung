package hust.soict.hedspi.aims.media;

public class DigitalVideoDisc extends Disc  implements Playable{
    private String director;
    private static int length;
    
    public String getDirector() {
        return director;
    }
    public void setDirector(String director) {
        this.director = director;
    }
    public int getLength() {
        return length;
    }
    public void setLength(int length) {
        this.length = length;
    }
    
    public DigitalVideoDisc(String id, String title, float cost) {
		super(id, title, cost, length, title);
	}
	
	public DigitalVideoDisc(String id, String title, int length, float cost) {
		super(id, title, cost, length, title);
		this.length = length;
	}
	
	public DigitalVideoDisc(String id, String title, String category, int length, float cost ) {
		super(id, title, category, cost);
		this.length = length;
	}
	
	public DigitalVideoDisc(String id, String title, String category, int length, String director, float cost) {
		this(id, title, category, length, cost);
		this.director = director;
	}
    public boolean Search(String string){
        //Tac title cua tham so can tim thanh cac tu
        //--> phuc vu cho viec xu li tim kiem
        int count = 0;

        String sTitle[] = title.split(" ");
        String getTitle[] = string.split(" ");
        for(int i = 0;  i < getTitle.length; i++){
            for(int j = 0; j < sTitle.length; j++){
                if(sTitle[j].equalsIgnoreCase(getTitle[i])){
                    count++;
                }
            }
        }

        if(count == sTitle.length)
            return true;
        return false;
    }
    @Override
    public void play() {
        // TODO Auto-generated method stub
        System.out.println("Playing DigitalVideoDisc: " + this.getTitle());
        System.out.println("DigitalVideoDisc length:  " + this.getLength());
    }
}
