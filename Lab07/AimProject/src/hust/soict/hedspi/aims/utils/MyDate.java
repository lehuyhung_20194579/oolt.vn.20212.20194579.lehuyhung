package hust.soict.hedspi.aims.utils;
import java.util.Calendar;
import java.util.TimeZone;

public class MyDate {
    private String day;
    private String month;
    private int year;

    //getter and setter
    public String getDay() {
        return day;
    }
    public void setDay(String day) {
        this.day = day;
    }
    public String getMonth() {
        return month;
    }
    public void setMonth(String month) {
        this.month = month;
    }
    public int getYear() {
        return year;
    }
    public void setYear(int year) {
        if(year > 0)
        this.year = year;
    }
    //create constructors
    public MyDate() {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        int current_day = calendar.get(Calendar.DATE);
        this.day = String.valueOf(current_day) + "th";
        int current_month = calendar.get(Calendar.MONTH) + 1;
        this.month = String.valueOf(current_month);
        this.year = calendar.get(Calendar.YEAR);
    }
    public void print() {
    }
}
